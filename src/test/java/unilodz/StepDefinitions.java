package unilodz;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.DriverManagerType;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StepDefinitions {

    private WebDriver driver;

    @Before
    public void setUp() {
        ChromeDriverManager.getInstance(DriverManagerType.CHROME).setup();
    }

    @Given("opened browser")
    public void opened_browser() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
    }

    //1 Main page test
    @When("enter main page")
    public void enter_main_page() {
        driver.get("http://wfi.uni.lodz.pl");
    }

    @Then("title should start with {string}")
    public void title_should_start_with(String title) {
        assertTrue(driver.getTitle().startsWith(title));
        driver.close();
    }

    //2 Kontakt Page test
    @When("enter main page and click on kontakt button")
    public void should_click_on_kontakt_button() {
        driver.get("http://wfi.uni.lodz.pl");
        driver.findElement(By.xpath("//*[@id=\"menu-item-4094\"]")).click();
    }

    @Then("url should end with {string}")
    public void should_redirect_to_kontakt_page(String string) {
        assertTrue(driver.getCurrentUrl().endsWith(string));
        driver.close();
    }

    //3 Usosweb page in new Tab test
    @When("enter main page and click on usosweb button")
    public void should_click_on_usosweb_button() {
        driver.get("http://wfi.uni.lodz.pl");
        driver.findElement(By.xpath("//*[@id=\"menu-item-510\"]")).click();
    }

    @Then("usos url should start with {string}")
    public void should_redirect_to_usosweb_page(String title) {
        ArrayList<String> browserTabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(browserTabs.get(1));
        assertTrue(driver.getCurrentUrl().startsWith(title));
        driver.quit();
    }

    //4 Aktualnosci page test
    @When("enter main page and click on 'aktualnosci' button")
    public void should_click_on_aktualnosci_button() {
        driver.get("http://wfi.uni.lodz.pl");
        WebElement element = driver.findElement(By.xpath("//*[@id=\"menu-item-507\"]/a"));

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        element.click();
    }

    @Then("aktualnosci url should end with {string}")
    public void should_redirect_to_aktualnosci_page(String string) {
        assertTrue(driver.getCurrentUrl().endsWith(string));
        driver.close();
    }

    //5 Wydzial page test
    @When("enter main page and click on 'wydzial' button")
    public void should_click_on_wydzial_button() {
        driver.get("http://wfi.uni.lodz.pl");
        driver.findElement(By.xpath("//*[@id=\"menu-item-2366\"]")).click();
    }

    @Then("wydzial url should end with {string}")
    public void should_redirect_to_wydzial_page(String string) {
        assertTrue(driver.getCurrentUrl().endsWith(string));
        driver.close();
    }

    //6 Student page test
    @When("enter main page and click on 'student' button")
    public void should_click_on_student_button() {
        driver.get("http://wfi.uni.lodz.pl");
        driver.findElement(By.xpath("//*[@id=\"menu-item-2361\"]")).click();
    }

    @Then("student url should end with {string}")
    public void should_redirect_to_student_page(String string) {
        assertTrue(driver.getCurrentUrl().endsWith(string));
        driver.close();
    }

    //7 Pracownik page test
    @When("enter main page and click on 'pracownik' button")
    public void should_click_on_pracownik_button() {
        driver.get("http://wfi.uni.lodz.pl");
        driver.findElement(By.xpath("//*[@id=\"menu-item-2356\"]")).click();
    }

    @Then("pracownik url should end with {string}")
    public void should_redirect_to_pracownik_page(String title) {
        assertTrue(driver.getCurrentUrl().endsWith(title));
        driver.close();
    }

    //8 Kandydat page test
    @When("enter main page and click on 'kandydat' button")
    public void should_click_on_kandydat_button() {
        driver.get("http://wfi.uni.lodz.pl");
        driver.findElement(By.xpath("//*[@id=\"menu-item-2351\"]")).click();
    }

    @Then("kandydat url should end with {string}")
    public void should_redirect_to_kandydat_page(String title) {
        assertTrue(driver.getCurrentUrl().endsWith(title));
        driver.close();
    }

    //9 Wspolpraca page test
    @When("enter main page and click on 'wspolpraca' button")
    public void should_click_on_wspolpraca_button() {
        driver.get("http://wfi.uni.lodz.pl");
        driver.findElement(By.xpath("//*[@id=\"menu-item-1339\"]")).click();
    }

    @Then("wspolpraca url should end with {string}")
    public void should_redirect_to_wspolpraca_page(String string) {
        assertTrue(driver.getCurrentUrl().endsWith(string));
        driver.close();
    }

    //10 Poczta page in new Tab test
    @When("enter main page and click on 'poczta' button")
    public void should_click_on_poczta_button() {
        driver.get("http://wfi.uni.lodz.pl");
        driver.findElement(By.xpath("//*[@id=\"menu-item-509\"]")).click();
    }

    @Then("poczta url should start with {string}")
    public void should_redirect_to_microsoft_login_page(String string) {
        ArrayList<String> browserTabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(browserTabs.get(1));
        assertTrue(driver.getCurrentUrl().startsWith(string));
        driver.quit();
    }

    //11 Kontakt search test
    @When("enter main page and click on search button and look for 'kontakt'")
    public void should_click_on_search_button_and_look_for_kontakt() {
        driver.get("http://wfi.uni.lodz.pl");
        WebElement element = driver.findElement(By.xpath("//*[@id=\"menu-item-search\"]"));
        element.click();

        WebElement element2 = driver.findElement(By.xpath("//*[@id=\"s\"]"));
        element2.sendKeys("kontakt");
        element2.submit();

    }

    @Then("search url should end with {string}")
    public void should_check_search_url(String string) {
        assertTrue(driver.getCurrentUrl().endsWith(string));
        driver.close();
    }

    //12 Footer test
    @Then("footer should contain address$")
    public void should_get_footer_and_check_address(String title) {
        WebElement element = driver.findElement(By.xpath("//*[@id=\"footer\"]"));
        assertEquals(title, element.getText());
        driver.close();
    }

    //13 Search for aktualnosci and check results
    @When("enter main page and click on search button and look for 'aktulanosci'")
    public void should_click_on_search_button() {
        driver.get("http://wfi.uni.lodz.pl");
        WebElement element = driver.findElement(By.xpath("//*[@id=\"menu-item-search\"]"));
        element.click();

        WebElement element2 = driver.findElement(By.xpath("//*[@id=\"s\"]"));
        element2.sendKeys("aktualnosci");
        element2.submit();
    }

    @Then("should return results$")
    public void should_search_for(String title) {
        WebElement element = driver.findElement(By.xpath("//*[@id=\"main\"]/div[1]/div/main"));
        String searchResults = StringUtils.substringAfter(element.getText(), "wyszukiwania dla: aktualnosci\n");
        assertEquals(title, searchResults);
        driver.close();
    }

    //14 facebook page in new tab test
    @When("enter main page and click on 'facebook' button")
    public void should_click_on_facebook_button() {
        driver.get("http://wfi.uni.lodz.pl");
        driver.findElement(By.xpath("//*[@id=\"wpfm-floating-menu-nav\"]/ul/li[3]/a")).click();
    }

    @Then("facebook url should end with {string}")
    public void should_redirect_to_facebook_page(String title) {
        ArrayList<String> browserTabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(browserTabs.get(1));
        assertTrue(driver.getCurrentUrl().endsWith(title));
        driver.quit();
    }


    //15 negative style
    @When("enter main page and click on 'narzedzia dostepnosci' button")
    public void should_click_on_narzedzia_dostepnosci_button() {
        driver.get("http://wfi.uni.lodz.pl");
        driver.findElement(By.xpath("//*[@id=\"pojo-a11y-toolbar\"]/div[1]")).click();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.findElement(By.xpath("//*[@id=\"pojo-a11y-toolbar\"]/div[2]/div/ul/li[4]")).click();
    }


    @Then("page should be in negative mode")
    public void should_check_if_page_is_in_negative_mode() {
        WebElement element = driver.findElement(By.xpath("//*[@id=\"pojo-a11y-toolbar\"]/div[2]/div/ul/li[4]/a"));
        String classAttr = element.getAttribute("class");
        assertTrue(classAttr.endsWith("negative-contrast active"));
        driver.close();
    }
}

