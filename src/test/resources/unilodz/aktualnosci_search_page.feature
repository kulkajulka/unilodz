Feature: Aktualnosci Page
  Scenario: Check number of results after searching for 'aktualnosci' Page
    Given opened browser
    When enter main page and click on search button and look for 'aktulanosci'
    Then should return results
        """
        1
        Aktualności
        2
        Studia doktoranckie
        3
        Projekty badawcze
        4
        Edukacja
        """