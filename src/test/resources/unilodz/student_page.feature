Feature: Student Page
  Scenario: Redirect to Student Page
    Given opened browser
    When enter main page and click on 'student' button
    Then student url should end with "/student/"