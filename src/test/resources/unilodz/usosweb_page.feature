Feature: Usosweb Page
  Scenario: Redirect to UsosWeb Page
    Given opened browser
    When enter main page and click on usosweb button
    Then usos url should start with "https://usosweb.uni.lodz.pl/"