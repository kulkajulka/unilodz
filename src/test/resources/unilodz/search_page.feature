Feature: Search Page
  Scenario: Redirect to Wspolpraca Page
    Given opened browser
    When enter main page and click on search button and look for 'kontakt'
    Then search url should end with "/?s=kontakt"