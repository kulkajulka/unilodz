Feature: Footer Check
  Scenario: Check if footer contains Name and Address of institution
    Given opened browser
    When enter main page
    Then footer should contain address
          """
          Wydział Fizyki i Informatyki Stosowanej
          Uniwersytetu Łódzkiego
          ul. Pomorska 149/153
          90-236 Łódź
          """
