Feature: Poczta Page
  Scenario: Redirect to MS Login Page
    Given opened browser
    When enter main page and click on 'poczta' button
    Then poczta url should start with "https://login.microsoftonline.com/"